# CI/CD Labs task 2

## Fork https://github.com/csuvikg/tap-ci
## Reimplement our CI/CD process in the selected platform:
## Joro: GitLab CI
## Niki: GitHub Actions
## Krasi: Travis CI

### Stages:

I have cloned the repository https://github.com/csuvikg/tap-ci , created new
project in GitLab and made initial commit.

```bash
commit 4ff4383ead8916994af8ba714cf8991159db9957
Author: Georgi Fuchedzhiev <georgifuchedzhiev@infinitelambda.com>
Date:   Thu Jan 6 16:12:58 2022 +0200

    Forking all files from https://github.com/csuvikg/tap-ci
```

Started reading about CI/CD here --> https://docs.gitlab.com/ee/ci/quick_start/

Shared runners for the CI/CD jobs were available and .gitlab-ci.yml is 
present by default in the root of the project.


